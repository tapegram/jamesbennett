export type Publication = {
  description: string;
  tags: Tag[];
  url: string;
  datePublished: string;
};

export type BlogPost = {
  title: string;
  content: string;
  date: string;
  tags: Tag[];
};

export type Contact = {
  url: string;
  name: string;
  platform: string;
};

export type Tag = {
  name: string;
  slug: string;
};

export type Content = {
  posts: BlogPost[];
  publications: Publication[];
};

export type Link = {
  name: string;
  url: string;
};

export const toPublication = (
  description: string,
  tags: Tag[],
  url: string,
  datePublished: string
): Publication => ({
  description,
  tags,
  url,
  datePublished,
});

export const toTag = (name: string, slug: string): Tag => ({
  name,
  slug,
});
