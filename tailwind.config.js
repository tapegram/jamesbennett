module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  container: {
    center: true,
  },
  variants: {
    extend: {},
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: false
  },
  theme: {
    extend: {
      fontFamily: {
        sans: ["roboto", "sans-serif"],
        serif: ["Merriweather", "serif"],
      },
    },
  },
};
