import { sayHello } from "./hello";

describe("say hello", () => {
  test("hello world", () => {
    expect(sayHello("World")).toBe("Hello, World!");
  });
  test("hello mom", () => {
    expect(sayHello("mom")).toBe("Hello, mom!");
  });
});
