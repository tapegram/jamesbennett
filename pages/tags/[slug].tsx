import * as T from "../../types";
import { getTaggedContent } from "../../server/graphcms";
import ContentFeed from "../../components/ContentFeed";
import Layout from "../../components/Layout";
import { navBarLinks } from "../../consts";

type Props = {
  posts: T.BlogPost[];
  publications: T.Publication[];
};

const Index = (props: Props) => (
  <Layout navBarLinks={navBarLinks}>
    <div>
      <ContentFeed posts={props.posts} publications={props.publications} />
    </div>
  </Layout>
);

export async function getStaticPaths() {
  const paths = [{ params: { slug: "classical-music" } }];
  return { paths, fallback: false };
}

export async function getStaticProps(context: any) {
  const { posts, publications } = await getTaggedContent(context.params.slug);
  return {
    props: {
      posts,
      publications,
    },
  };
}

export default Index;
