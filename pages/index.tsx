import Layout from "../components/Layout";
import Link from "next/link";

const IndexPage = () => (
  <Layout navBarLinks={[]}>
    <div className="grid grid-flow-row">
      <div className="mx-auto">
        <div className="avatar justify-center">
          <div className="w-1/2 rounded-full">
            <img src="/james-bennett-ii-crabs.jpg" />
          </div>
        </div>
      </div>
      <div className="flex justify-center mt-2">
        <Link href="/order/work" className="hover:text-red-700">
          order
        </Link>
        <p className="mx-1">or</p>
        <Link href="/chaos/blog" className="hover:text-red-700">chaos</Link>
      </div>
    </div>
  </Layout>
);

export default IndexPage;
