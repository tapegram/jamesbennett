import Blog from "../../../components/Blog";
import * as T from "../../../types";
import { getPosts } from "../../../server/graphcms";
import Layout from "../../../components/Layout";
import { navBarLinks } from "../../../consts";

type Props = {
  posts: T.BlogPost[];
};

const IndexPage = (props: Props) => (
  <Layout navBarLinks={navBarLinks}>
    <Blog posts={props.posts} />;
  </Layout>
);

export async function getStaticProps() {
  return {
    props: {
      posts: await getPosts(),
    },
  };
}

export default IndexPage;
