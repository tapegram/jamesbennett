import Bio from "../../../components/Bio";
import Layout from "../../../components/Layout";
import { getBio } from "../../../server/graphcms";
import { navBarLinks } from "../../../consts";

type Props = {
  bio: string;
};

const IndexPage = (props: Props) => (
  <Layout navBarLinks={navBarLinks}>
    <div className="m-4">
      <Bio bio={props.bio} />
    </div>
  </Layout>
);

export async function getStaticProps() {
  return {
    props: {
      bio: await getBio(),
    },
  };
}

export default IndexPage;
