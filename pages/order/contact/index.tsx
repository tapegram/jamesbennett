import * as T from "../../../types";
import Contact from "../../../components/Contact";
import Layout from "../../../components/Layout";
import { navBarLinks } from "../../../consts";

type Props = {
  contactMethods: T.Contact[];
};

const IndexPage = (props: Props) => (
  <Layout navBarLinks={navBarLinks}>
    <div className="m-4">
      {props.contactMethods.map((contact) => (
        <div key={contact.platform}>
          <Contact
            url={contact.url}
            name={contact.name}
            platform={contact.platform}
          />
        </div>
      ))}
    </div>
  </Layout>
);

export async function getStaticProps() {
  return {
    props: {
      contactMethods: toContactMethods(),
    },
  };
}

export default IndexPage;

const toContactMethods = () => [
  {
    url: "mailto:jamesiibennett@gmail.com",
    name: "jamesiibennett@gmail.com",
    platform: "email",
  },
  {
    url: "https://twitter.com/jamesabennettii",
    name: "@jamesabennettii",
    platform: "twitter",
  },
];
