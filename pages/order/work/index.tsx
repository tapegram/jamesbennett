import * as T from "../../../types";
import Publications from "../../../components/Publications";
import { getWorks } from "../../../server/graphcms";
import Layout from "../../../components/Layout";
import { navBarLinks } from "../../../consts";

type Props = {
  publications: T.Publication[];
};

const IndexPage = (props: Props) => (
  <Layout navBarLinks={navBarLinks}>
    <div className="center">
      <Publications publications={props.publications} />
    </div>
  </Layout>
);

export async function getStaticProps() {
  return {
    props: {
      publications: await getWorks(),
    },
  };
}

export default IndexPage;
