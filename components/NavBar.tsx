import Link from "next/link";
import * as T from "../types";

type Props = {
  links: T.Link[];
};

const NavBar = (props: Props) => (
  <div className="flex justify-center mt-2">
    {props.links.map((link, index) => (
      <>
        {index > 0 ? <p className="mx-1">/</p> : null}
        {toLink(link)}
      </>
    ))}
  </div>
);

const toLink = (link: T.Link) => (
  <Link href={link.url}>
    {link.name}
  </Link>
);

export default NavBar;
