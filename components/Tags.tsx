import * as T from "../types";
import Tag from "./Tag";

type Props = {
  tags: T.Tag[];
};

const Tags = (props: Props) => (
  <>
    {props.tags.map((tag) => (
      <Tag name={tag.name} slug={tag.slug} />
    ))}
  </>
);

export default Tags;
