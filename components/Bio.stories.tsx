import Bio from "./Bio";

export default {
  title: "Components/Bio",
  component: Bio,
};

export const ExampleBio = () => <Bio bio={toBioContent()} />;

const toBioContent = () =>
  "<p>James is a noted pineapple enthusiast and native of the original (read: Chesapeake) Bay Area. At the impressionable age of 17, he moved to New York in search of concrete pastures and scooped up a BA in history from Columbia University. Classical music had always been an interest, but it really switched on after he heard Yo-Yo Ma rip “Fear” from Astor Piazzolla’s <em>Four Tango Sensations</em>. It popped up on shuffle, and he listened to it no fewer than 40 times in a row.  He’s also game to talk about jazz, hip-hop, and wave (chill, synth, vapor, or otherwise). James is also fond of funky ales, fall foliage, all things nautical, and harsh winters. Some would say he has a passing interest in the post-romantic situational comedy <em>Friends</em>. </p>";
