type Props = {
  bio: string;
};

/*
We may wish to find a non dangerous way to do this. Gotta look at some blogs
*/
const Bio = (props: Props) => (
  <div dangerouslySetInnerHTML={{ __html: props.bio }}></div>
);

export default Bio;
