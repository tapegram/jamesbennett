import * as T from "../types";
import BlogPost from "./BlogPost";

type Props = {
  posts: T.BlogPost[];
};

const Blog = (props: Props) => (
  <div className="m-4">
    {props.posts.map((post, index) => (
      <div key={index} className="m-5">
        <BlogPost
          tags={post.tags}
          title={post.title}
          content={post.content}
          date={post.date}
        />
      </div>
    ))}
  </div>
);

export default Blog;
