import * as T from "../types";
import Link from "next/link";

type Props = T.Tag;

const Tag = (props: Props) => (
  <Link href={`/tags/${props.slug}`}>
    {props.name}
  </Link>
);
export default Tag;
