import * as T from "../types";
import Tags from "./Tags";

export default {
  title: "Components/Tags",
  component: Tags,
};

export const NoTags = () => <Tags tags={[]} />;
export const OneTag = () => (
  <Tags tags={[toTag("classical music", "classical-music")]} />
);
export const TwoTag = () => (
  <Tags
    tags={[
      toTag("classical music", "classical-music"),
      toTag("cooking", "cooking"),
    ]}
  />
);

const toTag = (name: string, slug: string): T.Tag => ({
  name,
  slug,
});
