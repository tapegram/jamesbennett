import Link from "next/link";

type Props = {
  url: string;
  name: string;
  platform: string;
};

const Contact = (props: Props) => (
  <div>
    <Link href={props.url}>
      <div>
        {props.platform} | {props.name}
      </div>
    </Link>
  </div>
);

export default Contact;
