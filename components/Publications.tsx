import Publication from "./Publication";
import * as T from "../types";

type Props = {
  publications: T.Publication[];
};

const Publications = (props: Props) => (
  <div className="grid md:grid-flow-row md:grid-cols-3 gap-4 m-4">
    {props.publications.map((publication) => (
      <Publication
        description={publication.description}
        tags={publication.tags}
        url={publication.url}
      />
    ))}
  </div>
);

export default Publications;
