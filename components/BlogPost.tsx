import * as T from "../types";
import Tags from "./Tags";

type Props = {
  title: string;
  date: string;
  content: string;
  tags: T.Tag[];
};

const BlogPost = (props: Props) => (
  <div>
    <div className="text-lg">{props.title}</div>
    <Tags tags={props.tags} />
    <div className="text-xs">{props.date}</div>
    <div
      dangerouslySetInnerHTML={{ __html: props.content }}
      className="text-base"
    />
  </div>
);

export default BlogPost;
