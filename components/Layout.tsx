import React, { FunctionComponent } from "react";
import NavBar from "./NavBar";
import * as T from "../types";

type Props = {
  navBarLinks: T.Link[];
  children: React.ReactNode;
};

const Layout: FunctionComponent<Props> = ({ navBarLinks, children }) => (
  <div className="container mx-auto">
    <NavBar links={navBarLinks} />
    <div className="container flex justify-center mx-auto">{children}</div>
  </div>
);

export default Layout;
