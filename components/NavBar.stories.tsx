import NavBar from "./NavBar";

export default {
  title: "Components/NarBar",
  component: NavBar,
};

export const Example = () => (
  <NavBar
    links={[
      { name: "blog", url: "/blog" },
      { name: "articles", url: "/articles" },
      { name: "appearances", url: "/appearances" },
    ]}
  />
);
