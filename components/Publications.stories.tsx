import Publications from "./Publications";
import * as T from "../types";

export default {
  title: "Components/Publications",
  component: Publications,
};

export const NoPublications = () => <Publications publications={[]} />;
export const OnePublication = () => (
  <Publications
    publications={[
      T.toPublication(
        "we need to be listening to reber's fourth symphony way more often",
        [T.toTag("classical music", "classical-music")],
        "http://www.wqxr.org/story/why-rebers-fourth-symphony-so-rare/",
        "2021-02-01"
      ),
    ]}
  />
);
export const FullScreen = () => (
  <Publications
    publications={[
      T.toPublication(
        "we need to be listening to reber's fourth symphony way more often",
        [T.toTag("classical music", "classical-music")],
        "http://www.wqxr.org/story/why-rebers-fourth-symphony-so-rare/",
        "2021-02-01"
      ),
      T.toPublication(
        "recognize the light and handle the glare",
        [T.toTag("classical music", "classical-music")],
        "http://www.wqxr.org/story/recognize-light-and-handle-glare/",
        "2021-02-01"
      ),
      T.toPublication(
        "the word is bonds",
        [T.toTag("classical music", "classical-music")],
        "http://www.wqxr.org/story/margaret-bonds-heard-music-word/",
        "2021-02-01"
      ),
      T.toPublication(
        "where are the orchestra's guitars?",
        [T.toTag("classical music", "classical-music")],
        "http://www.wqxr.org/story/where-are-orchestras-guitars/",
        "2021-02-01"
      ),
      T.toPublication(
        "countdown 2020: the takeaways",
        [T.toTag("classical music", "classical-music")],
        "http://www.wqxr.org/story/countdown-2020-takeaways/",
        "2021-02-01"
      ),
      T.toPublication(
        "hold on -- why is 'handsel and gretel' a christmas piece?",
        [T.toTag("classical music", "classical-music")],
        "http://www.wqxr.org/story/new-york-city-classical-christmas/",
        "2021-02-01"
      ),
      T.toPublication(
        "even with quiet concert halls, beethoven talk still dominated 2020",
        [T.toTag("classical music", "classical-music")],
        "http://www.wqxr.org/story/even-quiet-concert-halls-beethoven-talk-still-dominated-2020/",
        "2021-02-01"
      ),
      T.toPublication(
        "big fan: 5 beethoven dedicatees",
        [T.toTag("classical music", "classical-music")],
        "http://www.wqxr.org/story/5-pieces-beethoven-dedicated-people/",
        "2021-02-01"
      ),
      T.toPublication(
        "sound of silk",
        [T.toTag("classical music", "classical-music")],
        "http://www.wqxr.org/story/hear-me-out-silk/",
        "2021-02-01"
      ),
      T.toPublication(
        "2020. it's been a year",
        [T.toTag("classical music", "classical-music")],
        "http://www.wqxr.org/story/2020-its-been-year-artist-reflections/",
        "2021-02-01"
      ),
    ]}
  />
);

export const SmallScreen = () => (
  <div className="w-1/4">
    <Publications
      publications={[
        T.toPublication(
          "we need to be listening to reber's fourth symphony way more often",
          [T.toTag("classical music", "classical-music")],
          "http://www.wqxr.org/story/why-rebers-fourth-symphony-so-rare/",
          "2021-02-01"
        ),
        T.toPublication(
          "recognize the light and handle the glare",
          [T.toTag("classical music", "classical-music")],
          "http://www.wqxr.org/story/recognize-light-and-handle-glare/",
          "2021-02-01"
        ),
        T.toPublication(
          "the word is bonds",
          [T.toTag("classical music", "classical-music")],
          "http://www.wqxr.org/story/margaret-bonds-heard-music-word/",
          "2021-02-01"
        ),
        T.toPublication(
          "where are the orchestra's guitars?",
          [T.toTag("classical music", "classical-music")],
          "http://www.wqxr.org/story/where-are-orchestras-guitars/",
          "2021-02-01"
        ),
        T.toPublication(
          "countdown 2020: the takeaways",
          [T.toTag("classical music", "classical-music")],
          "http://www.wqxr.org/story/countdown-2020-takeaways/",
          "2021-02-01"
        ),
        T.toPublication(
          "hold on -- why is 'handsel and gretel' a christmas piece?",
          [T.toTag("classical music", "classical-music")],
          "http://www.wqxr.org/story/new-york-city-classical-christmas/",
          "2021-02-01"
        ),
        T.toPublication(
          "even with quiet concert halls, beethoven talk still dominated 2020",
          [T.toTag("classical music", "classical-music")],
          "http://www.wqxr.org/story/even-quiet-concert-halls-beethoven-talk-still-dominated-2020/",
          "2021-02-01"
        ),
        T.toPublication(
          "big fan: 5 beethoven dedicatees",
          [T.toTag("classical music", "classical-music")],
          "http://www.wqxr.org/story/5-pieces-beethoven-dedicated-people/",
          "2021-02-01"
        ),
        T.toPublication(
          "sound of silk",
          [T.toTag("classical music", "classical-music")],
          "http://www.wqxr.org/story/hear-me-out-silk/",
          "2021-02-01"
        ),
        T.toPublication(
          "2020. it's been a year",
          [T.toTag("classical music", "classical-music")],
          "http://www.wqxr.org/story/2020-its-been-year-artist-reflections/",
          "2021-02-01"
        ),
      ]}
    />
  </div>
);
