import * as T from "../types";
import BlogPost from "./BlogPost";
import Publication from "./Publication";

type Props = T.Content;

const ContentFeed = (props: Props) => (
  <div className="m-4">
    {toElements(
      sortByDate(
        fromPosts(props.posts).concat(fromPublications(props.publications))
      )
    )}
  </div>
);

type Item = {
  type: string;
  date: string;
  content: T.BlogPost | T.Publication;
};

const fromPost = (post: T.BlogPost): Item => ({
  type: "post",
  date: post.date,
  content: post,
});

const fromPosts = (posts: T.BlogPost[]): Item[] => posts.map(fromPost);

const fromPublication = (publication: T.Publication): Item => ({
  type: "publication",
  date: publication.datePublished,
  content: publication,
});

const fromPublications = (publications: T.Publication[]): Item[] =>
  publications.map(fromPublication);

const sortByDate = (items: Item[]): Item[] =>
  items.sort((a, b) => (b.date < a.date ? -1 : 1));

const toElement = (item: Item) =>
  item.type === "post"
    ? toBlogPost(item.content as T.BlogPost)
    : toPublication(item.content as T.Publication);

const toElements = (items: Item[]) => items.map(toElement);

const toBlogPost = (post: T.BlogPost) => (
  <div className="m-5">
    <BlogPost
      tags={post.tags}
      title={post.title}
      content={post.content}
      date={post.date}
    />
  </div>
);

const toPublication = (publication: T.Publication) => (
  <div className="m-5">
    <Publication
      tags={publication.tags}
      description={publication.description}
      url={publication.url}
    />
  </div>
);

export default ContentFeed;
