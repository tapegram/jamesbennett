import Tag from "./Tag";

export default {
  title: "Components/Tag",
  component: Tag,
};

export const ExampleTag = () => (
  <Tag name="classical music" slug="classical-music" />
);
