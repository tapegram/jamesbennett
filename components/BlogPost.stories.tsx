import * as T from "../types";
import BlogPost from "./BlogPost";

export default {
  title: "Components/BlogPost",
  component: BlogPost,
};

export const BlogPostExample = () => (
  <BlogPost
    title="a whole new world"
    content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    date="1/10/2021"
    tags={[toTag("classical music", "classical-music")]}
  />
);

export const NoTags = () => (
  <BlogPost
    title="a whole new world"
    content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    date="1/10/2021"
    tags={[]}
  />
);

export const MultipleTags = () => (
  <BlogPost
    title="a whole new world"
    content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    date="1/10/2021"
    tags={[
      toTag("classical music", "classical-music"),
      toTag("cooking", "cooking"),
    ]}
  />
);

const toTag = (name: string, slug: string): T.Tag => ({
  name,
  slug,
});
