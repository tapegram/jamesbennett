import Contact from "./Contact";

export default {
  title: "Components/Contact",
  component: Contact,
};

export const Twitter = () => (
  <Contact
    url="https://twitter.com/jamesabennettii"
    name="@jamesabennettii"
    platform="twitter"
  />
);

export const Email = () => (
  <Contact
    url="mailto:jamesiibennett@gmail.com"
    name="jamesiibennett@gamil.com"
    platform="email"
  />
);
