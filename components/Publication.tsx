import Link from "next/link";
import * as T from "../types";
import Tags from "./Tags";

type Props = {
  description: string;
  tags: T.Tag[];
  url: string;
};

const Publication = (props: Props) => (
  <div>
    <Tags tags={props.tags} />
    <Link href={props.url}>
      <div dangerouslySetInnerHTML={{ __html: props.description }} />
    </Link>
  </div>
);

export default Publication;
