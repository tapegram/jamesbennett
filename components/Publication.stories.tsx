import Publication from "./Publication";
import * as T from "../types";

export default {
  title: "Components/Publication",
  component: Publication,
};

export const ExamplePublication = () => (
  <Publication
    description="we need to be listening to reber's fourth symphony way more often"
    tags={[toTag("classical music", "classical-music")]}
    url="http://www.wqxr.org/story/why-rebers-fourth-symphony-so-rare/"
  />
);

export const MultipleTags = () => (
  <Publication
    description="we need to be listening to reber's fourth symphony way more often"
    tags={[
      toTag("classical music", "classical-music"),
      toTag("cooking", "cooking"),
    ]}
    url="http://www.wqxr.org/story/why-rebers-fourth-symphony-so-rare/"
  />
);

const toTag = (name: string, slug: string): T.Tag => ({
  name,
  slug,
});
