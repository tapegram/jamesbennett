import * as T from "../types";
import Blog from "./Blog";

export default {
  title: "Components/Blog",
  component: Blog,
};

export const BlogExample = () => <Blog posts={toPosts()} />;

const toPosts = (): T.BlogPost[] => [
  {
    title: "a whole new world",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    date: "1/10/2021",
    tags: [toTag("classical music", "classical-music")],
  },
  {
    title: "under the sea",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    date: "12/11/1991",
    tags: [toTag("classical music", "classical-music")],
  },
  {
    title: "youre welcome",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    date: "09/05/1999",
    tags: [
      toTag("classical music", "classical-music"),
      toTag("cooking", "cooking"),
    ],
  },
];

const toTag = (name: string, slug: string): T.Tag => ({
  name,
  slug,
});
