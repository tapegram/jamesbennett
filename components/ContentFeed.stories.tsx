import * as T from "../types";
import ContentFeed from "./ContentFeed";

export default {
  title: "Components/ContentFeed",
  component: ContentFeed,
};

export const ExampleFeed = () => (
  <ContentFeed posts={toPosts()} publications={toPublications()} />
);

const toPosts = (): T.BlogPost[] => [
  {
    title: "a whole new world",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    date: "2021-01-01",
    tags: [T.toTag("classical music", "classical-music")],
  },
  {
    title: "under the sea",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    date: "2021-03-01",
    tags: [T.toTag("classical music", "classical-music")],
  },
  {
    title: "youre welcome",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    date: "2021-06-01",
    tags: [
      T.toTag("classical music", "classical-music"),
      T.toTag("cooking", "cooking"),
    ],
  },
];

const toPublications = (): T.Publication[] => [
  T.toPublication(
    "we need to be listening to reber's fourth symphony way more often",
    [T.toTag("classical music", "classical-music")],
    "http://www.wqxr.org/story/why-rebers-fourth-symphony-so-rare/",
    "2021-05-01"
  ),
  T.toPublication(
    "recognize the light and handle the glare",
    [T.toTag("classical music", "classical-music")],
    "http://www.wqxr.org/story/recognize-light-and-handle-glare/",
    "2021-02-01"
  ),
  T.toPublication(
    "the word is bonds",
    [T.toTag("classical music", "classical-music")],
    "http://www.wqxr.org/story/margaret-bonds-heard-music-word/",
    "2021-05-01"
  ),
  T.toPublication(
    "where are the orchestra's guitars?",
    [T.toTag("classical music", "classical-music")],
    "http://www.wqxr.org/story/where-are-orchestras-guitars/",
    "2021-02-01"
  ),
  T.toPublication(
    "countdown 2020: the takeaways",
    [T.toTag("classical music", "classical-music")],
    "http://www.wqxr.org/story/countdown-2020-takeaways/",
    "2021-02-01"
  ),
  T.toPublication(
    "hold on -- why is 'handsel and gretel' a christmas piece?",
    [T.toTag("classical music", "classical-music")],
    "http://www.wqxr.org/story/new-york-city-classical-christmas/",
    "2021-02-01"
  ),
  T.toPublication(
    "even with quiet concert halls, beethoven talk still dominated 2020",
    [T.toTag("classical music", "classical-music")],
    "http://www.wqxr.org/story/even-quiet-concert-halls-beethoven-talk-still-dominated-2020/",
    "2021-02-01"
  ),
  T.toPublication(
    "big fan: 5 beethoven dedicatees",
    [T.toTag("classical music", "classical-music")],
    "http://www.wqxr.org/story/5-pieces-beethoven-dedicated-people/",
    "2021-02-01"
  ),
  T.toPublication(
    "sound of silk",
    [T.toTag("classical music", "classical-music")],
    "http://www.wqxr.org/story/hear-me-out-silk/",
    "2021-01-01"
  ),
  T.toPublication(
    "2020. it's been a year",
    [T.toTag("classical music", "classical-music")],
    "http://www.wqxr.org/story/2020-its-been-year-artist-reflections/",
    "2021-01-01"
  ),
];
