import * as T from "./types";

export const navBarLinks: T.Link[] = [
  {
    name: "work",
    url: "/order/work",
  },
  {
    name: "blog",
    url: "/chaos/blog",
  },
  {
    name: "bio",
    url: "/order/bio",
  },
  {
    name: "contact",
    url: "/order/contact",
  },
];
