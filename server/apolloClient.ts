import { ApolloClient, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
  uri: "https://us-east-1.cdn.hygraph.com/content/ckku86at7093m01z74cxdhogo/master",
  cache: new InMemoryCache(),
});

export default client;
