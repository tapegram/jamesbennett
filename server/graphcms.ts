import client from "./apolloClient";
import { gql } from "@apollo/client";
import * as T from "../types";

export const getWorks = async (): Promise<T.Publication[]> => {
  const { data } = await client.query({
    query: gql`
      query GetWorks {
        works(stage: PUBLISHED, locales: en) {
          title {
            html
          }
          tags {
            name
            slug
          }
          url
          datePublished
        }
      }
    `,
  });

  return toPublications(data.works);
};

export const getPosts = async (): Promise<T.BlogPost[]> => {
  const { data } = await client.query({
    query: gql`
      query GetPosts {
        posts(stage: PUBLISHED, locales: en) {
          title
          content {
            html
          }
          tags {
            name
            slug
          }
          date
        }
      }
    `,
  });

  return toPosts(data.posts);
};

export const getBio = async (): Promise<string> => {
  const { data } = await client.query({
    query: gql`
      query GetBios {
        bios(stage: PUBLISHED, locales: en) {
          content {
            html
          }
        }
      }
    `,
  });

  return data.bios[0].content.html;
};

export const getTaggedContent = async (slug: string): Promise<T.Content> => {
  const { data } = await client.query({
    variables: {
      slug,
    },
    query: gql`
      query Tags($slug: String!) {
        tag(where: { slug: $slug }, stage: PUBLISHED, locales: en) {
          name
          slug
          works {
            title {
              html
            }
            url
            tags {
              name
              slug
            }
            datePublished
          }
          posts {
            content {
              html
            }
            title
            date
            tags {
              name
              slug
            }
          }
        }
      }
    `,
  });
  return {
    posts: toPosts(data.tag.posts),
    publications: toPublications(data.tag.works),
  };
};

const toPublications = (works: any[]): T.Publication[] =>
  works.map(toPublication);

const toPublication = (work: any): T.Publication => ({
  description: work.title.html,
  tags: toTags(work.tags),
  url: work.url,
  datePublished: work.datePublished,
});

const toTags = (tags: any[]): T.Tag[] => tags.map(toTag);
const toTag = (tag: any): T.Tag => ({
  name: tag.name,
  slug: tag.slug,
});

const toPosts = (posts: any[]): T.BlogPost[] => posts.map(toPost);
const toPost = (post: any): T.BlogPost => ({
  title: post.title,
  content: post.content.html,
  date: post.date,
  tags: toTags(post.tags),
});
